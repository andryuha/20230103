#============== Provider variables ==============#
variable "sa_key_file" {
  description = "Path to service account key file"
  type        = string
}
variable "cloud_id" {
  description = "The Cloud ID"
  type        = string
}
variable "folder_id" {
  description = "The Folder ID"
  type        = string
}
variable "zone" {
  description = "The availability zone"
  type        = string
  default     = "ru-central1-a"
}
#============== Bucket variables ==============#
variable "bucket-sa" {
  description = "Service account name for object storage"
  type    = string
  default = "bucket-sa"
}
variable "backend-bucket" {
  description = "Object storage name"
  type    = string
  default = "backend-tf-bucket"
}
