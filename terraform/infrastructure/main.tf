module "network" {
  source              = "git@github.com:Andryuhamag/terraform-yc-network.git?ref=v0.1"
  network_name        = var.network_name
  network_description = var.network_description
  subnets             = var.swarm_subnets
}

module "nodes" {
  for_each      = var.swarm_cluster
  source        = "git@github.com:Andryuhamag/terraform-yc-compute.git"
  zone          = each.value["zone"]
  imade_id      = var.image_id
  platform_id   = var.platform_id
  name          = each.value["name"]
  hostname      = each.value["hostname"]
  label_name    = each.value["label_name"]
  subnet_id     = lookup({ for mapItem in module.network.subnets_info : mapItem.zone => mapItem.subnet_id }, each.value["zone"], null)
  cores         = var.node_cores
  core_fraction = var.node_core_fraction
  memory        = var.node_memory
  disk_size     = var.node_disc_size
  ssh-keys      = "${var.compute_user}:${file("${var.compute_ssh_key}")}"
  sg_id         = module.network.sg_id
  nat           = var.nat
}

resource "local_file" "inventory" {
  content = templatefile("templates/${var.inventory_template}", {
    hostnames  = values(module.nodes)[*].hostname
    ipaddr     = values(module.nodes)[*].nat_ip_address
    label_name = values(module.nodes)[*].label_name
  })
  filename = var.inventory_file
}
