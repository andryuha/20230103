output "public_IP" {
  value = [for node in module.nodes : node.nat_ip_address]
}

output "private_IP" {
  value = [for node in module.nodes : node.ip_address]
}
