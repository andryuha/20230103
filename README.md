# Object Storage MinIO on Docker Swarm

## Данный проект разработан исключительно в образовательных целях.

`docker-compose.yaml` и `nginx.conf` взял с [официального репозитория](https://github.com/minio/minio/tree/master/docs/orchestration/docker-compose).

Результатом запуска данного проекта является Docker Swarm кластер из 4 нод с запущенным MinIO кластером, доступным по адресу http://ipaddress:9001, где `ipaddress` можно будет получить после развертывания инфраструктуры из каталога `terraform/infrastructure`.

Инфраструктура разворачивается в `Yandex Cloud` с помощью `terraform`.

Параметры, необходимые для успешного развертывания инфрасруктуры:
| Файл                                                | Параметр          | Описание                                                                 |
|-----------------------------------------------------|-------------------|--------------------------------------------------------------------------|
| `terraform/backend/terraform.tfvars.example`, `terraform/infrastructure/terraform.tfvars.example`        | `sa_key_file`     | Путь до авторизованного ключа `key.json` Вашего сервисного аккаунта      |
| `terraform/backend/terraform.tfvars.example`, `terraform/infrastructure/terraform.tfvars.example`        | `cloud_id`        | ID Вашего облака в YC                                                    |
| `terraform/backend/terraform.tfvars.example`, `terraform/infrastructure/terraform.tfvars.example`       | `folder_id`       | ID Вашего каталога в YC                                                  |
| `terraform/infrastructure/terraform.tfvars.example` | `image_id`        | ID образа виртуальной машины. По умолчанию установлен Ubuntu 20.04       |
| `terraform/infrastructure/terraform.tfvars.example` | `compute_ssh_key` | Путь до Вашего публичного ключа для доступа к виртуальным машинам        |
| `terraform/infrastructure/backend.conf.example`     | `access_key`      | Идентификатор статического ключа доступа к Object Storage                |
| `terraform/infrastructure/backend.conf.example`     | `secret_key`      | Секретный ключ доступа к Object Storage                                  |

Для этого необходимо внести нужные параметры в файлы:
```
- terraform/backend/terraform.tfvars.example

- terraform/infrastructure/terraform.tfvars.example

- terraform/infrastructure/backend.conf.example
```
 Затем переименовать:
```
- terraform/backend/terraform.tfvars

- terraform/infrastructure/terraform.tfvars

- terraform/infrastructure/backend.conf
```

Далее создать bucket для хранения файла состояния `terraform.tfstate` в `Yandex Object Storage`:
```
$ cd terraform/backend

$ terraform init

$ terraform apply --auto-approve
```

Далее развернуть 4 сервера:
```
$ cd ../infrastructure

$ terraform init -backend-config=backend.conf

$ terraform apply --auto-approve

```

После того, как сервера будут готовы к работе, в корневом каталоге проекта выполнить следующие команды:
```
ansible-galaxy install -r requirements.yml

ansible-playbook playbook.yml
```
