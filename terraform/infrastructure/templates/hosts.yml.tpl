swarm_nodes:
  hosts:
%{ for index, hostname in hostnames ~}
    ${hostname}:
      ansible_host: ${ipaddr[index]}
%{ endfor ~}

swarm_managers:
  hosts:
%{ for index, hostname in hostnames ~}
%{~ if length(regexall("manager", label_name[index])) > 0 ~}
    ${hostname}:
      ansible_host: ${ipaddr[index]}
%{ endif ~}
%{ endfor ~}

swarm_workers:
  hosts:
%{ for index, hostname in hostnames ~}
%{~ if length(regexall("worker", label_name[index])) > 0 ~}
    ${hostname}:
      ansible_host: ${ipaddr[index]}
%{ endif ~}
%{ endfor ~}