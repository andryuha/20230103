#============== Provider variables ==============#
variable "sa_key_file" {
  description = "Path to service account key file"
  type        = string
}

variable "cloud_id" {
  description = "The Cloud ID"
  type        = string
}

variable "folder_id" {
  description = "The Folder ID"
  type        = string
}

#=============== Network variables ===============#
variable "network_name" {
  description = "Network name"
  type        = string
  default     = "default-network"
}

variable "network_description" {
  description = "Network description"
  type        = string
  default     = "Default network"
}

variable "sg_name" {
  description = "Security group name"
  type        = string
  default     = "default-sg"
}

variable "swarm_subnets" {

  type = list(
    object(
      {
        name        = string,
        description = string,
        zone        = string,
        cidr        = list(string)
      }
    )
  )
}

#=============== Instance variables ===============#
variable "image_id" {
  description = "Image ID"
  type        = string
}

variable "platform_id" {
  description = "The type of virtual machine to create."
  type        = string
  default     = "standard-v1"
}

variable "node_cores" {
  type = number
  default = 2
}

variable "node_core_fraction" {
  type = number
  default = 20
}

variable "node_memory" {
  type = number
  default = 2
}

variable "node_disc_size" {
  type = number
  default = 20
}

variable "compute_user" {
  description = "User name of instance"
  type        = string
  default     = "ubuntu"
}

variable "compute_ssh_key" {
  description = "Public ssh-key for instance"
  type        = string
  default     = "~/.ssh/id_rsa.pub"
}

variable "nat" {
  description = "Public IP addres over NAT"
  type        = bool
  default     = false
}

variable "swarm_cluster" {
  type = map(any)
  default = {
    master = {
      name       = "node1"
      hostname   = "node1"
      zone       = "ru-central1-a"
      label_name = "manager"
    }
    worker1 = {
      name       = "node2"
      hostname   = "node2"
      zone       = "ru-central1-b"
      label_name = "worker"
    }
    worker2 = {
      name       = "node3"
      hostname   = "node3"
      zone       = "ru-central1-d"
      label_name = "worker"
    }
    worker3 = {
      name       = "node4"
      hostname   = "node4"
      zone       = "ru-central1-a"
      label_name = "worker"
    }
  }
}

#============= Provisioning variables =============#
variable "inventory_template" {
  description = "Inventory template file name"
  type        = string
  default     = "hosts.yml.tpl"
}

variable "inventory_file" {
  description = "Inventory file name"
  type        = string
  default     = "hosts.yml"
}